#include <stdio.h>
int main()
{
	int a[10][10],r,c,i,j,n,average,sum=0;
	printf("Enter the number of rows and columns = ");
	scanf("%d%d",&r,&c);
	printf("Enter the elements ");
	for(i=0;i<r;i++)
	{
		for(j=0;j<c;j++)
			scanf("%d",&a[i][j]);
	}
	printf("The transpose of the matrix is \n");
	for(i=0;i<r;i++)
	{
		printf("\n");
		for(j=0;j<c;j++)
			printf("%d",a[j][i]);
	}
	
	return 0 ;
}
